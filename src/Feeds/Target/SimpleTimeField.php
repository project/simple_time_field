<?php

namespace Drupal\simple_time_field\Feeds\Target;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a Simple Time Field mapper.
 *
 * @FeedsTarget(
 *   id = "simple_time_field",
 *   field_types = {"simple_time_type"}
 * )
 */
class SimpleTimeField extends FieldTargetBase {

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    if ($values['value']) {
      $values['value'] = $this->prepareTimeValue($values['value']);
    }
  }

  /**
   * Prepares a time value.
   *
   * @param string $value
   *   The time value to convert.
   *
   * @return string
   *   A time string, possibly adjusted to the correct format for storage.
   */
  protected function prepareTimeValue(string $value): string {
    $time = DrupalDateTime::createFromFormat('H:i', $this->normalizeTimeInput($value));
    if ($time instanceof DrupalDateTime && !$time->hasErrors()) {
      return $time->format('H:i');
    }

    return '';
  }

  /**
   * Normalize time input from source.
   *
   * @param string $value
   *   The time that was provided via the feeds source.
   *
   * @return string
   *   Normalized time ready to be converted into DrupalDateTime.
   */
  protected function normalizeTimeInput(string $value): string {
    // Parse and check if time is an actual time.
    $time = '';
    $timestamp = strtotime($value);
    if ($timestamp !== FALSE && $timestamp > strtotime('1970-01-01')) {
      $time = date('H:i', $timestamp);
    }
    return $time;
  }

}
