<?php

namespace Drupal\simple_time_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Time' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_time_formatter_three",
 *   label = @Translation("Time field three"),
 *   field_types = {
 *     "simple_time_type"
 *   }
 * )
 */
class SimpleTimeFormatterThree extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $current_time = date('H:i');
    $summary[] = $this->t('24-hour format of an hour with leading zeros 00 through 23 (@time)', ['@time' => $current_time]);
    return $summary;
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      $date = date("Y-m-d $item->value:00");
      $markup = date("H:i", strtotime($date));

      // Render each element as markup.
      $element[$delta] = ['#markup' => $markup];
    }

    return $element;
  }

}
