# Simple Time Field
### Objective:
Simple Time Field is a drupal 9 module that provides the simple time field widget.

### Features:
This Module will show Proficiency is following areas of Module Development.

* Plugin Ins
* FieldFormatter
* FieldType
* FieldWidget

### Set Up:
1. To Setup, Like any Drupal 9 module Just Place the Module Files in Module Directory
2. Enable the module "Simple Time Field" through Extends Admin Tab
3. Once Enabled a field **Simple Time Field** would be Available in the manage fields for the contents and entities
4. Add the field on the content types you wish to add the time field
5. You can configure the manage display to show the formats to display the time.
	* The available options for the time field on the manage display are:
	* Lowercase Ante meridiem and Post meridiem
	* Uppercase Ante meridiem and Post meridiem
	* 24-hour format of an hour with leading zeros 00 through 23



